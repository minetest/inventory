<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');

$authParam = (config('app.env') !== 'local')
    ? ['register' => false]
    : [];

Auth::routes($authParam);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/inventory/check', 'CheckInventoryController@index')->name('checkInventory');
Route::get('/inventory/check/searchPlayer/{player}', 'CheckInventoryController@searchPlayer')->name('checkInventorySearchPlayer');
Route::get('/inventory/check/showInventory/{player}', 'CheckInventoryController@showInventory')->name('checkInventoryShowInventory');
