# Minetest Invotenry

Appli web permettant l'inspection de l'inventair d'un joueur développée en PHP avec le framework Laravel.

## Demo

Une demo de l'aplication est disponible à l'adresse suivante : http://inventory.akiba.fr
<pre>
Identifiant : admin@admin.com 
Mot de passe : admin123
</pre>

## Installation

<pre>
git clone https://gitea.com/minetest/inventory.git
cd inventory
composer install --no-dev
find storage/ -type d -exec chmod 0777 {} \;
chmod 0777 bootstrap/cache/
cp .env.example .env
php artisan key:generate
</pre>

Editer le fichier .env se trouvant à la racine de l'application et indiquer le chemin vers le fichier 
players.sqlite. Attention aux droits d'accès à ce fichier, faire en sorte qu'il soit accessible en écriture
ainsi que le dossier ou il se trouve.

Ensuite :
<pre>
php artisan migrate
</pre>

## Utilisation



