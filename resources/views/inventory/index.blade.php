@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('inventory/index.str_0') }}</div>

                <div class="card-body">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('inventory/index.str_1') }}</span>
                        </div>

                        <input type="text" class="form-control" id="playerSearch">

                        <div class="input-group-append">
                            <button class="btn btn-outline-primary" type="button" onclick="inventoryCheck()">
                                {{ __('inventory/index.str_2') }}
                            </button>
                        </div>
                    </div>

                    <div class="row" id="playersList" style="display: none">
                        <div class="col-md-4">
                            <select class="form-control" size="10" id="player" onclick="showInventory()"></select>
                        </div>

                        <div class="col-md-8" id="inventoryList" style="display: none">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="col-3">Type d'inventaire</th>
                                        <th class="col-1"></th>
                                        <th class="col-8">Item</th>
                                        <th class="col-8">Qte.</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <span id="message"></span>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_file')
<script src="{{ asset('js/inventory/check.js') }}"></script>
@endsection
