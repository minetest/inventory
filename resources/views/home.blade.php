@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('home.str_0') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('checkInventory') }}" class="btn btn-outline-info">
                        <i class="fa fa-th-large"></i> {{ __('home.str_1') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
