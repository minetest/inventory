<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                    => 'These credentials do not match our records.',
    'throttle'                  => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'                     => 'Login',
    'logout'                    => 'Logout',
    'email_address'             => 'Email address',
    'password'                  => 'Password',
    'remember_me'               => 'Remember me',
    'forgot_password'           => 'Forgot Your Password?',
    'register'                  => 'Register',
    'name'                      => 'Name',
    'confirm_password'          => 'Confirm Password',
    'verify_email'              => 'Verify Your Email Address',
    'verif_link_send'           => 'A fresh verification link has been sent to your email address.',
    'check_email_link'          => 'Before proceeding, please check your email for a verification link.',
    'receive_email'             => 'If you did not receive the email',
    'another_request'           => 'click here to request another',
    'confirm_before_continuing' => 'Please confirm your password before continuing.',
    'reset_password'            => 'Reset Password',
    'send_reset_link'           => 'Send Password Reset Link',

];
