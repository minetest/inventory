<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                    => 'Erreur de connection.',
    'throttle'                  => 'Trop de tentatives de connexion. Merci de ré-essayer dans :seconds secondes.',
    'login'                     => 'Connexion',
    'logout'                    => 'Déconnexion',
    'email_address'             => 'Adresse email',
    'password'                  => 'Mot de passe',
    'remember_me'               => 'Rester connecté',
    'forgot_password'           => 'Mot de passe oublié ?',
    'register'                  => 'S\'enregister',
    'name'                      => 'Nom',
    'confirm_password'          => 'Confirmer le mot de passe',
    'verify_email'              => 'Vérifiez votre adresse e-mail',
    'verif_link_send'           => 'Un nouveau lien de confirmation a été envoyé à votre adresse e-mail.',
    'check_email_link'          => 'Avant de continuer, veuillez vérifier votre e-mail pour le lien de confirmation.',
    'receive_email'             => 'Si vous n\'avez pas reçu l\'e-mail',
    'another_request'           => 'cliquez ici pour en demander un autre',
    'confirm_before_continuing' => 'Veuillez confirmer votre mot de passe avant de continuer.',
    'reset_password'            => 'Réinitialiser le mot de passe',
    'send_reset_link'           => 'Envoyer le lien de réinitialisation du mot de passe',

];
