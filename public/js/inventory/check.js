
/* ---------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function inventoryCheck()
{
    let player = $('#playerSearch').val()

    $.get(route('checkInventorySearchPlayer', player))
        .done(function(playersList)
        {
            let playersListHtml = ''
            playersList = $.parseJSON(playersList)

            if (playersList.length > 0)
            {
                $.each(playersList, function(k, v) {
                    playersListHtml += '<option value="' + v['name'] + '">' + v['name'] + '</option>'
                })

                $('#playersList').show().find('select').html(playersListHtml)
                $('#message').text('')
            }
            else
            {
                $('#playersList').hide()
                $('#message').text('Pas de joueurs trouvés avec le terme saisi')
            }
        })
        .fail(function()
        {
            alert('inventoryCheck() : error')
        })

}


/* ---------------------------------------------------------------------------------------------------------------------------------------------------
 *
 */
function showInventory()
{
    let player = $('#playersList').find('select').val()

    $.get(route('checkInventoryShowInventory', player))
        .done(function(inventoryList)
        {
            let inventoryListHtml = ''
            inventoryList = $.parseJSON(inventoryList)

            if (inventoryList.length > 0)
            {
                let itemImage = ''

                $.each(inventoryList, function(k, v)
                {
                    itemImage = (v['item_image'] !== '')
                        ? '<img src="/images/items/' + v['item_image'] + '" class="item-image">'
                        : ''

                    inventoryListHtml += '<tr>' +
                        '<td>' + v['inv_name'] + '</td>' +
                        '<td>' + itemImage + '</td>' +
                        '<td>' + v['item_name'] + '</td>' +
                        '<td>' + v['item_qte'] + '</td>' +
                    '</tr>'
                })

                $('#inventoryList').show().find('tbody').html(inventoryListHtml)
            }
            else
            {
                $('#inventoryList').hide()
            }
        })
        .fail(function()
        {
            alert('showInventory() : error')
        })
}
