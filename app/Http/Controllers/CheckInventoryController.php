<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckInventoryController extends Controller
{
    /*
     * @access  public
     */
    public function index()
    {
        return view('inventory.index');
    }


    /* -----------------------------------------------------------------------------------------------------------------------------------------------
     * @access  public
     * @return  string
     */
    public function searchPlayer(Request $request) : string
    {
        activity()
            ->withProperties([
                'IP' => $_SERVER['REMOTE_ADDR'],
                'player' => $request->player
            ])
            ->log('Recherche joueur');

        $players = DB::select(
            'SELECT name FROM player WHERE LOWER(name) LIKE LOWER(:player) ORDER BY name',
            ['player' => '%' . $request->player . '%']
        );

        $players = (count($players) > 0)
            ? $players
            : [];

        return json_encode($players);
    }


    /* -----------------------------------------------------------------------------------------------------------------------------------------------
     * @access  public
     * @return  string
     */
    public function showInventory(Request $request) : string
    {
        activity()
            ->withProperties([
                'IP' => $_SERVER['REMOTE_ADDR'],
                'player' => $request->player
            ])
            ->log('Affichage inventaire joueur');

        $inventory = DB::select(
            'SELECT t.item, t.slot_id, i.inv_name
             FROM player_inventory_items AS t
                 LEFT JOIN player_inventories AS i
                     ON i.inv_id = t.inv_id
                     AND i.player = t.player
             WHERE LOWER(t.player) = LOWER(:player)
                 AND i.inv_name != \'hunger\'
             ORDER BY t.inv_id, t.slot_id',
            ['player' => $request->player]
        );

        if (count($inventory) > 0)
            foreach ($inventory as $k => $v)
            {
                $item = explode(' ', $inventory[$k]->item);

                $inventory[$k]->item_name  = $item[0];
                $inventory[$k]->item_qte   = !empty($item[1]) ? $item[1] : '';
                $inventory[$k]->item_image = $this->getItemImage($inventory[$k]->item);
            }
        else
            $inventory = [];

        return json_encode($inventory);
    }


    /* -----------------------------------------------------------------------------------------------------------------------------------------------
     * @param  string  $itemString
     * @access  private
     * @return  string
     */
    private function getItemImage(string $itemString) : string
    {
        if (empty($itemString))
            return '';

        $itemArray = explode(' ', $itemString);
        $itemData  = explode(':', $itemArray[0]);
        $itemImage = $itemData[0] . '/' . $itemData[1] . '.png';


        return (file_exists(public_path('images/items/' . $itemImage)))
            ? $itemImage
            : '';
    }
}
